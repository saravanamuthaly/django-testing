from pprint import pprint

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpRequest
from django.db.models import Q
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from .models import Room, Topic, User
from .forms import RoomForm


def login_page(request: HttpRequest):
    next_page = request.GET.get("next")

    if request.user.is_authenticated:
        return redirect("home_page")

    if request.method == "POST":
        username = request.POST.get("username").lower()
        password = request.POST.get("password")

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)

            if next_page is not None:
                return redirect(next_page)

            return redirect("home_page")
        else:
            messages.error(request, "Username or password is wrong.")

    context = {}
    return render(request, "base/login_form.html", context)


def logout_page(request: HttpRequest):
    logout(request)
    return redirect("home_page")


def register_page(request: HttpRequest):
    if request.user.is_authenticated:
        return redirect("home_page")

    register_form = UserCreationForm()

    if request.method == "POST":
        form = UserCreationForm(request.POST)

        if form.is_valid():
            user: User = form.save(commit=False)
            user.username = user.username.lower()
            user.save()

            login(request, user)

            return redirect("home_page")
        else:
            messages.error(request, "Error while registering new user")

    context = {"form": register_form}
    return render(request, "base/register_form.html", context)


def home_view(request: HttpRequest):
    q = request.GET.get(key="q", default=None)

    if q is None:
        rooms = Room.objects.all()
    else:
        # & - and, | - or
        rooms = Room.objects.filter(
            Q(topic__name__icontains=q)
            | Q(name__icontains=q)
            | Q(description__icontains=q)
        )

    rooms_count = rooms.count()
    topics = Topic.objects.all()

    try:
        selected_topic = Topic.objects.get(name=q)
    except Topic.DoesNotExist:
        selected_topic = None

    context = {
        "rooms": rooms,
        "topics": topics,
        "rooms_count": rooms_count,
        'selected_topic': selected_topic,
    }
    return render(request, "base/home.html", context)


def room_view(request: HttpRequest, pk: int):
    room = Room.objects.get(id=pk)
    room_messages = room.message_set.all()

    context = {"room": room, 'room_messages': room_messages}

    return render(request, "base/room.html", context)


@login_required(login_url=login_page)
def create_room(request: HttpRequest):
    # check if request has a user

    form = RoomForm()

    if request.method == "POST":
        form = RoomForm(request.POST)
        if form.is_valid():
            # check if topic is null, then check if new topic has a value
            if form.cleaned_data["topic"] is None:
                if form.cleaned_data["new_topic"] != "":
                    new_topic = form.cleaned_data["new_topic"]

                    topic = Topic(name=new_topic)

                    topic.save()

                    room = Room(
                        name=form.cleaned_data["name"],
                        topic=topic,
                        description=form.cleaned_data["description"],
                        host=request.user,
                    )

                    room.save()
            else:
                room = Room(
                    name=form.cleaned_data["name"],
                    topic=form.cleaned_data["topic"],
                    description=form.cleaned_data["description"],
                    host=request.user,
                )
                room.save()

            return redirect("home_page")

    context = {"form": form}

    return render(request, "base/room_form.html", context)


@login_required(login_url=login_page)
def update_room(request: HttpRequest, pk):
    room = Room.objects.get(id=pk)
    form = RoomForm(instance=room)

    if room.host != request.user:
        return HttpResponse("You are not allowed here")

    if request.method == "POST":
        form = RoomForm(request.POST, instance=room)
        if form.is_valid():
            form.save()
            return redirect("home_page")

    context = {"form": form}

    return render(request, "base/room_form.html", context)


@login_required(login_url=login_page)
def delete_room(request: HttpRequest, pk):
    room = Room.objects.get(id=pk)

    if room.host != request.user:
        return redirect("home_page")

    if request.method == "POST":
        room.delete()
        return redirect("home_page")

    context = {"obj": room}

    return render(request, "base/delete.html", context)
