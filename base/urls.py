from django.urls import path
from . import views

urlpatterns = [
    path('', views.home_view, name='home_page'),
    path('register/', views.register_page, name='register'),
    path('login/', views.login_page, name='login'),
    path('logout/', views.logout_page, name='logout'),

    path('room/<int:pk>', views.room_view, name='room_page'),
    path('create-room/', views.create_room, name='create_room'),
    path('edit-room/<int:pk>', views.update_room, name='update_room'),
    path('delete-room/<int:pk>', views.delete_room, name='delete_room'),
]
