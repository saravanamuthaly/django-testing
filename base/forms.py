from django.forms import ModelForm, CharField

from .models import Room


class RoomForm(ModelForm):
    new_topic = CharField(required=False)

    class Meta:
        model = Room
        fields = ['name', 'topic', 'description']
