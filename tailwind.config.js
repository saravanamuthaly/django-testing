/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
      "./static/styles/input.css",
      "./templates/**/*.html",
      "./base/templates/**/*.html",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

